-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: mobile_suit_db
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bandos`
--

DROP TABLE IF EXISTS `bandos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bandos` (
  `bando_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `nombre` enum('Principado de Zeon','Federacion Terrestre') CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Principado de Zeon',
  `descripcion` text,
  PRIMARY KEY (`bando_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bandos`
--

LOCK TABLES `bandos` WRITE;
/*!40000 ALTER TABLE `bandos` DISABLE KEYS */;
INSERT INTO `bandos` VALUES (1,'Principado de Zeon','El Principado de Zeon, también conocido como el Ducado de Zeon, es una nación que aparece en Mobile Suit Gundam. Toma el control de las colonias de Side 3 y lucha contra la Federación Terrestre durante la Guerra de un Año.'),(2,'Federacion Terrestre','La Federación de la Tierra es un gobierno global presentado en la serie de televisión Mobile Suit Gundam. La Federación es una de las principales facciones de la línea de tiempo Universal Century.');
/*!40000 ALTER TABLE `bandos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_table`
--

DROP TABLE IF EXISTS `log_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_action` varchar(30) DEFAULT NULL,
  `module` varchar(30) DEFAULT NULL,
  `modelo` varchar(30) DEFAULT NULL,
  `ms_name` varchar(30) DEFAULT NULL,
  `usable` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_table`
--

LOCK TABLES `log_table` WRITE;
/*!40000 ALTER TABLE `log_table` DISABLE KEYS */;
INSERT INTO `log_table` VALUES (1,'Insert','Like','MS','05_Zaku',1),(2,'Insert','Like','MS','05_Zaku',1),(3,'Insert','Like','MS','05_Zaku',1),(4,'Insert','Like','MS','06_Zaku2',1);
/*!40000 ALTER TABLE `log_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobile_suit`
--

DROP TABLE IF EXISTS `mobile_suit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_suit` (
  `ms_modelo_id` int(11) NOT NULL,
  `ms_name` varchar(20) NOT NULL,
  `tamaño` varchar(20) DEFAULT NULL,
  `tamaño_Total` varchar(20) DEFAULT NULL,
  `peso_Total` varchar(20) DEFAULT NULL,
  `peso_Vacio` varchar(20) DEFAULT NULL,
  `velocidad` varchar(20) DEFAULT NULL,
  `energia` varchar(20) DEFAULT NULL,
  `encendido` varchar(20) DEFAULT NULL,
  `rango_Sensor` varchar(20) DEFAULT NULL,
  `pilots` enum('1','2') DEFAULT '1',
  `price_per_unit` varchar(20) NOT NULL,
  `production_date` varchar(10) NOT NULL,
  PRIMARY KEY (`ms_modelo_id`,`ms_name`),
  CONSTRAINT `fk_ms_modelo` FOREIGN KEY (`ms_modelo_id`) REFERENCES `modelo` (`modelo_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobile_suit`
--

LOCK TABLES `mobile_suit` WRITE;
/*!40000 ALTER TABLE `mobile_suit` DISABLE KEYS */;
INSERT INTO `mobile_suit` VALUES (4,'G_Cannon','1','1','1','1','1','100','1','1','1','200','28/02/2019'),(6,'05_Zaku','1','1','1','1','1','1','1','1','1','1','27/02/2019'),(6,'06_Zaku2','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(7,'03C_HY-GOGG','1','1','1','1','1','1','1','1','1','900','28/02/2019'),(7,'07_Z\'Gok','1','1','1','1','1','1','1','1','1','190','28/02/2019');
/*!40000 ALTER TABLE `mobile_suit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelo`
--

DROP TABLE IF EXISTS `modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelo` (
  `modelo_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(10) NOT NULL,
  `bando` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`modelo_id`),
  UNIQUE KEY `AK_nombre_modelo` (`nombre`),
  KEY `fk_bando_modelo` (`bando`),
  CONSTRAINT `fk_bando_modelo` FOREIGN KEY (`bando`) REFERENCES `bandos` (`bando_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelo`
--

LOCK TABLES `modelo` WRITE;
/*!40000 ALTER TABLE `modelo` DISABLE KEYS */;
INSERT INTO `modelo` VALUES (1,'AMS',1),(2,'AMX',1),(3,'CCMS',1),(4,'F71',2),(5,'LM111E',2),(6,'MS',1),(7,'MSM',1),(8,'MSS',2),(9,'MSZ',2);
/*!40000 ALTER TABLE `modelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plantilla_historial_productos`
--

DROP TABLE IF EXISTS `plantilla_historial_productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plantilla_historial_productos` (
  `id_compra` int(11) NOT NULL,
  `ms_modelo_id` int(11) NOT NULL,
  `ms_name` varchar(20) NOT NULL,
  `cantidad` varchar(20) DEFAULT NULL,
  `precio` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_compra`,`ms_modelo_id`,`ms_name`),
  KEY `fk_history` (`ms_modelo_id`,`ms_name`),
  CONSTRAINT `fk_history` FOREIGN KEY (`ms_modelo_id`, `ms_name`) REFERENCES `mobile_suit` (`ms_modelo_id`, `ms_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plantilla_historial_productos`
--

LOCK TABLES `plantilla_historial_productos` WRITE;
/*!40000 ALTER TABLE `plantilla_historial_productos` DISABLE KEYS */;
/*!40000 ALTER TABLE `plantilla_historial_productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platilla_historial_compras`
--

DROP TABLE IF EXISTS `platilla_historial_compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platilla_historial_compras` (
  `id_compra` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `purchase_date` varchar(10) NOT NULL,
  PRIMARY KEY (`id_compra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platilla_historial_compras`
--

LOCK TABLES `platilla_historial_compras` WRITE;
/*!40000 ALTER TABLE `platilla_historial_compras` DISABLE KEYS */;
/*!40000 ALTER TABLE `platilla_historial_compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_action_table`
--

DROP TABLE IF EXISTS `social_action_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_action_table` (
  `user_id` smallint(6) NOT NULL,
  `ms_modelo_id` int(11) NOT NULL,
  `ms_name` varchar(20) NOT NULL,
  `typeOfAction` enum('like','dislike') NOT NULL,
  PRIMARY KEY (`user_id`,`ms_modelo_id`,`ms_name`,`typeOfAction`),
  KEY `fk_social_action_references1` (`ms_modelo_id`,`ms_name`),
  CONSTRAINT `fk_social_action_references1` FOREIGN KEY (`ms_modelo_id`, `ms_name`) REFERENCES `mobile_suit` (`ms_modelo_id`, `ms_name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_social_action_references2` FOREIGN KEY (`user_id`) REFERENCES `usuarios` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_action_table`
--

LOCK TABLES `social_action_table` WRITE;
/*!40000 ALTER TABLE `social_action_table` DISABLE KEYS */;
INSERT INTO `social_action_table` VALUES (2,6,'05_Zaku','like'),(2,6,'06_Zaku2','like');
/*!40000 ALTER TABLE `social_action_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_shop_cart`
--

DROP TABLE IF EXISTS `temp_shop_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_shop_cart` (
  `ms_modelo_id` int(11) NOT NULL,
  `ms_name` varchar(20) NOT NULL,
  `cantidad` varchar(20) DEFAULT NULL,
  `precio` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ms_modelo_id`,`ms_name`),
  CONSTRAINT `fk_ms_product` FOREIGN KEY (`ms_modelo_id`, `ms_name`) REFERENCES `mobile_suit` (`ms_modelo_id`, `ms_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_shop_cart`
--

LOCK TABLES `temp_shop_cart` WRITE;
/*!40000 ALTER TABLE `temp_shop_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_shop_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `user_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `user_pass` longtext,
  `user_mail` varchar(80) DEFAULT NULL,
  `avatar` longtext,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`,`user_mail`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Amuro Ray','$2y$10$kfDKKOCQg07gZ1STK6pgCOcpTVUu4/A7VY0U5IFajc56v6G6lLXJ2','cpardoca3@gmail.com','../info/Fotos/profile_pics/pic_icon2.png'),(2,'cristian','$2y$10$PLn5ebEYfXABqGB6voO/5.okXmI4LeaHdP2pyGd/7ESkopm6OudIu','cpardoca3@gmail.com','../info/Fotos/profile_pics/pic_icon3.png'),(3,'cristian2','$2y$10$S5ha/vJh/bzWSVwg6qeMU.ZlExIH6aPJ0ID0xZt/tYqcbSu/WplDe','cpardoca3@gmail.com',NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'mobile_suit_db'
--

--
-- Dumping routines for database 'mobile_suit_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-12 16:22:45

<?php
    include("view/inc/top_page_ms.php");
    session_start();
    session_regenerate_id();
    $pages="";
    $menus="";
    if(isset($_SESSION["type"])){
        //var_dump($_SESSION["user"]);
        $user_type=$_SESSION["type"];
        if ($user_type=="0") {
            $pages="view/inc/pages/user_pages.php";
            $menus="view/inc/menus/loged_menu.php";
        } elseif($user_type=="1") {
            $pages="view/inc/pages/admin_pages.php";
            $menus="view/inc/menus/admin_loged_menu.php";
        }
    }else{
        $pages="view/inc/pages/default_pages.php";
        $menus="view/inc/menus/menu.php";
    }
    
?>
    <div id="wrapper">		
        <div id="header">    	
            <?php include("view/inc/header.php");?>        
        </div>  
        <div id="menu">
            <?php  include($menus);?>
        </div>	
        <div id="">
            <?php include($pages);?>        
            <br style="clear:both;" />
        </div>
        <div id="footer">   	   
            <?php include("view/inc/footer.php");?>        
        </div>
    </div>
    <?php include("view/inc/bottom_page.php");?>

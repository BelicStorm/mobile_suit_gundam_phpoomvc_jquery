function insert_like(modelo,name,user){ //inserta el like en base de datos
    //console.log('insertando');
    $.ajax({ 
        type: 'GET', 
        url: "module/social_accions/controller/controller_social.php?op=like&modelo=" 
                    + modelo + "&name=" + name + "&user="+user, 
        data: { get_param: 'value' }, 
        dataType: 'json'
        
    })
    .done(function( data, textStatus, jqXHR ) {
        window.location.reload();
    })
    .fail(function(data, jqXHR, textStatus, errorThrown ) {
        if ( console && console.log ) {
            console.log( "La solicitud a fallado: " +  textStatus);
        }
   });
} 
function unlike(modelo,name,user){ //borra el like de base de datos
    $.ajax({ 
        type: 'GET', 
        url: "module/social_accions/controller/controller_social.php?op=unlike&modelo=" 
                    + modelo + "&name=" + name + "&user="+user, 
        data: { get_param: 'value' }, 
        dataType: 'json'
        
    })
    .done(function( data, textStatus, jqXHR ) {
        window.location.reload();
    })
    .fail(function(data, jqXHR, textStatus, errorThrown ) {
        if ( console && console.log ) {
            console.log( "La solicitud a fallado: " +  textStatus);
        }
   });
} 
function paint_liked_button(data,name,modelo){//pinta los likes dependiendo del usuario
    var x;
    
    user_logued.then(function(user){
        //console.log(user);
        for (i in data) {
            x = data[i].user_name;
            //console.log(x);
            if(x==user){
                $('.'+modelo+'.'+name).css('color','red');
            }
        }  
    })
        
}
var user_logued = new Promise(function(resolve){ //obten el usuario logeado
        $.ajax({ 
                type: 'GET', 
                url: "module/login_register/controller/autentication_controller.php?op=get_user_name", 
                data: { get_param: 'value' }, 
                dataType: 'json'
                
            })
            .done(function( result, textStatus, jqXHR ) {
                //console.log(result);
               resolve(result) ;
            })
        
});
function print_like(){ //imprime los likes de base de datos y resalta los del usuario logeado
         $(".like").each(function(){
            var modelo = $(this).attr('class').split(' ')[2];
            var name = $(this).attr('class').split(' ')[3];
            //console.log(modelo);
            //console.log(name);
            if(modelo!="undefined"){
                $.ajax({ 
                type: 'GET', 
                url: "module/social_accions/controller/controller_social.php?op=show_like&modelo=" + modelo + "&name=" + name, 
                data: { get_param: 'value' }, 
                dataType: 'json'
                
            })
            .done(function( data, textStatus, jqXHR ) {
                (data)==0
                    ? $('.'+modelo+'.'+name).html("Likes: 0")
                    : paint_liked_button(data,name,modelo)
                    $('.'+modelo+'.'+name).html("Likes:"+data.length);
            })
            .fail(function(data, jqXHR, textStatus, errorThrown ) {
                if ( console && console.log ) {
                    console.log( "La solicitud a fallado: " +  textStatus);
                }
            });
            }
        });
}
function control_and_call_insert_like(modelo,name,user){
    $.ajax({ 
        type: 'GET', 
        url: "module/social_accions/controller/controller_social.php?op=control_like&modelo=" 
                    + modelo + "&name=" + name + "&user="+user, 
        data: { get_param: 'value' }, 
        dataType: 'json'
        
    })
    .done(function( data, textStatus, jqXHR ) {
        //console.log('ok');
        //console.log(data);
        (data)==0
        ? //console.log('insert') 
            insert_like(modelo,name,user)
        :   unlike(modelo,name,user);
    })
    .fail(function(data, jqXHR, textStatus, errorThrown ) {
        if ( console && console.log ) {
            console.log( "La solicitud a fallado: " +  textStatus);
        }
    });
}
var consultas_no_logued_users = function(option) {
    return new Promise(function(resolve, reject) {
        $.ajax({ 
            type: 'GET', 
            url: "module/login_register/controller/no_logued_actions_controller.php?op="+option, 
            data: { get_param: 'value' }, 
            dataType: 'json'
        })
        .done(function( data, textStatus, jqXHR ) {
            //console.log('ok');
            resolve(data);
        })
    })
};
function no_loged_user_likes() {
    if(window.location != "http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_phpoomvc_jquery/Pagina(New-template)/index.php?page=login_register"){
       //console.log("hola");
        consultas_no_logued_users('Select_Like').then(function(result){
            //console.log(result);
            user_logued.then(function(user){
                if(result.usable==0){
                    if(user!="Ningun usuario en session"){
                        console.log("insertar like y usable a 1");
                        control_and_call_insert_like(result.modelo,result.ms_name,user);
                        consultas_no_logued_users('UnUse_Like').then(function(unuse){
                            return 0;
                        })
                    }else{
                        console.log("usable a 1");
                        consultas_no_logued_users('UnUse_Like').then(function(unuse){
                            return 0;
                        })
                    }
                }
            })
        })
    }
}
$(window).on('load',function(){//evento que comprueba si el item ya tiene like o no
    no_loged_user_likes();
    print_like();
    $(".like").on('click',function(){
            var modelo = $(this).attr('class').split(' ')[2];
            var name = $(this).attr('class').split(' ')[3];
        user_logued.then(function(user){
            if(user!="Ningun usuario en session"){
                control_and_call_insert_like(modelo,name,user);
            }else{
                console.log("Nop");
                    url = "index.php?page=login_register";
                    $( location ).attr("href", url);
                $.ajax({ 
                    type: 'GET', 
                    url: "module/login_register/controller/no_logued_actions_controller.php?op=Like&modelo=" 
                                + modelo + "&name=" + name, 
                    data: { get_param: 'value' }, 
                    dataType: 'json'
                    
                })
            }
        });
    });
});



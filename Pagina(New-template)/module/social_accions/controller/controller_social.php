<?php
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_phpoomvc_jquery/Pagina(New-template)/';
include($path . "module/social_accions/model/social_DAOMS.php");

switch ($_GET['op']) {
   
    case 'show_like':
        try{
            $daoms = new socialDAOMS();
            $rdo = $daoms->show_likes($_GET['modelo'],$_GET['name']);
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        if(!$rdo){
            echo json_encode(0);
            exit;
        }else{
            $dinfo = array();
				foreach ($rdo as $row) {
					array_push($dinfo, $row);
                }
				echo json_encode($dinfo);
            exit;
        }
        break;
    case 'control_like':
        try{
            $daoms = new socialDAOMS();
            $rdo = $daoms->control_like($_GET['modelo'],$_GET['name'],$_GET['user']);
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
           echo json_encode($rdo);
        break;
    case 'like':
        try{
            $daoms = new socialDAOMS();
            $rdo = $daoms->unlike_like($_GET['modelo'],$_GET['name'],$_GET['user'],0);
            echo json_encode("ok");
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        
        break;
    case 'unlike':
        try{
            $daoms = new socialDAOMS();
            $rdo = $daoms->unlike_like($_GET['modelo'],$_GET['name'],$_GET['user'],1);
            echo json_encode("ok");
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        break;
    
    
        default:
        include('view/inc/error404.php');
        break;
}
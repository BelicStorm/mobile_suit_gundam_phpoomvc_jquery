$(document).ready(function(){ 
       var url= "module/all_products/controller/all_products_controller.php?op=datatable"; 
        var source =
        {
            
            dataType: "json",
            dataFields: [
                { name: 'Modelo', type: 'string' },
                { name: 'Nombre', type: 'string' },
                { name: 'Productor', type: 'string' }
            ],
            id: 'id',
            url: url
        };
        
        var dataAdapter = new $.jqx.dataAdapter(source);
        if($("#all_prpoducts_datatable").length !=0){
            $("#all_prpoducts_datatable").jqxDataTable(
                {
                    pageable: true,
                    pagerButtonsCount: 10,
                    source: dataAdapter,
                    sortable: true,
                    pageable: true,
                    filterable: true,
                    pagerMode: 'advanced',
                    columns: [
                      { text: 'Modelo', dataField: 'Modelo',width: 250},
                      { text: 'Nombre', dataField: 'Nombre',width: 250 },
                      { text: 'Productor', dataField: 'Productor',width: 250 }
                  ]
                });  
        }
});
<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_phpoomvc_jquery/Pagina(New-template)/';
    include($path . "model/connect.php");
//include("model/connect.php");
    class DAOMS{
        //funciones crud
        function select_all($order){
            if($order==""){
                $sql = "SELECT modelo.nombre as Modelo,
                                mobile_suit.ms_name as Nombre,
                                bandos.nombre as Productor
                        from mobile_suit inner join modelo on mobile_suit.ms_modelo_id = modelo.modelo_id
                                         inner join bandos on modelo.bando=bandos.bando_id;";
            }else{
                $sql = "SELECT modelo.nombre as Modelo,
                                mobile_suit.ms_name as Nombre,
                                bandos.nombre as Productor
                        from mobile_suit inner join modelo on mobile_suit.ms_modelo_id = modelo.modelo_id
                                         inner join bandos on modelo.bando=bandos.bando_id
                        where bandos.bando_id = $order;";
            }
            $conexion = conect_disconect::con();
            $res = mysqli_query($conexion, $sql);
            conect_disconect::close($conexion);
            return $res;
        }
        function select_ms($name){
			$sql = "SELECT * FROM mobile_suit WHERE ms_name='$name'";
			
			$conexion = conect_disconect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            conect_disconect::close($conexion);
            return $res;
        }
        function insert_mobile_suit($model){
            //die(var_dump($model));
            $conexion = conect_disconect::con();
            $sql= "INSERT into mobile_suit values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $res = mysqli_stmt_init($conexion);
            if (mysqli_stmt_prepare($res,$sql) !== false) {
                mysqli_stmt_bind_param($res, "sssssssssssss", 
                $model['modelo'], $model['nombre'],$model['tamaño'],
                $model['tamaño_total'],$model['peso_t'],$model['peso_vacio'],
                $model['velocidad'], $model['energia'],$model['encendido'],
                $model['rango_sensor'],$model['pilots'], $model['price_per_unit'],$model['date']);
                mysqli_execute($res);
                conect_disconect::close($conexion);
            }
            else{
                echo "Error a";
            }
            
            return $res;
        }
        function update_mobile_suit($model,$modelo,$name){
            //die(var_dump($model).var_dump($modelo."".$name));
            $conexion = conect_disconect::con();
            $sql="UPDATE mobile_suit 
            inner join modelo on mobile_suit.ms_modelo_id = modelo.modelo_id					
            set ms_modelo_id=?, 
                    ms_name=?,
                    tamaño=?,
                    tamaño_Total=?,
                    peso_Total=?,
                    peso_Vacio=?,
                    velocidad=?,
                    energia=?,
                    encendido=?,
                    rango_Sensor=?,
                    pilots=?,
                    price_per_unit=?,
                    production_date=?
                 where modelo.nombre=? and ms_name=?;";
                    //die(var_dump($sql));
            $res = mysqli_stmt_init($conexion); 
            if (mysqli_stmt_prepare($res,$sql) !== false) {
                mysqli_stmt_bind_param($res, "sssssssssssssss", 
                $model['modelo'], $model['nombre'],$model['tamaño'],
                $model['tamaño_total'],$model['peso_t'],$model['peso_vacio'],
                $model['velocidad'], $model['energia'],$model['encendido'],
                $model['rango_sensor'],$model['pilots'], $model['price_per_unit'],$model['date'],$modelo,$name);
                mysqli_execute($res);
               conect_disconect::close($conexion);
            }
                    
                    return $res;
            
            
        }
        function delete_mobile_suit($modelo,$name){
            $conexion = conect_disconect::con();
            $sql = "DELETE mobile_suit from mobile_suit inner join modelo on mobile_suit.ms_modelo_id = modelo.modelo_id
                                                        inner join bandos on modelo.bando=bandos.bando_id
                                       where ms_name = ? and modelo.nombre =?;";
                $sentencia = mysqli_stmt_init($conexion);
                if (mysqli_stmt_prepare($sentencia, $sql) !== false) {
                    mysqli_stmt_bind_param($sentencia, "ss", $name, $modelo);
                    mysqli_execute($sentencia);
                    echo "Filas afectadas:" . mysqli_stmt_affected_rows($sentencia) . "</br>";
                    conect_disconect::close($conexion);
                    return $sentencia;
                }
                
        }
        function delete_all_mobile_suit(){
            $sql = "DELETE from mobile_suit;";
			
			$conexion = conect_disconect::con();
            $res = mysqli_query($conexion, $sql);
            conect_disconect::close($conexion);
            return $res;
        }
        //funciones de validacion
       
    }

?>
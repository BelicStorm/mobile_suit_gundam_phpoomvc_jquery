<?php
	function validate_model(){
		$error='';
		$filtro = array(
			'modelo'=> array(),
			'nombre' => array(),
			'tamaño' => array(),
			'tamaño_total' => array(),
			'peso_t' => array(),
			'peso_vacio' => array(),
			'velocidad' => array(),
			'energia' => array(),
			'encendido' => array(),
			'rango_sensor' => array(),
			'pilots' => array(),
			'price_per_unit' => array(),
			'date' => array(
				'filter' => FILTER_VALIDATE_REGEXP,
				'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
			)
		);
		
		$resultado=filter_input_array(INPUT_POST,$filtro);
		if(db_validate_name_model($resultado['nombre'],$resultado['modelo'])>0){
			$error="El modelo ya tiene registrado un MS con ese nombre";
		}elseif($resultado['tamaño_total']<$resultado['tamaño']){
			$error='El tamaño no  puede ser mayor al tamaño total del MS';
		}elseif($resultado['peso_t']<$resultado['peso_vacio']){
			$error='Inserte adecuadamente el peso del MS';
		}elseif(!$resultado['date']){
			$error='Formato fecha dd/mm/yy';	
		}else{
			 return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
		};
		return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
	};

	function db_validate_name_model($name,$model){
		$conexion = conect_disconect::con();
		$sql="SELECT ms_modelo_id,ms_name from mobile_suit where ms_modelo_id=? and ms_name=?;";
		$sentencia = mysqli_stmt_init($conexion);
			if (mysqli_stmt_prepare($sentencia, $sql) !== false) {
				mysqli_stmt_bind_param($sentencia, "ss", $model, $name);
				mysqli_execute($sentencia);
				$res=mysqli_stmt_affected_rows($sentencia);
				conect_disconect::close($conexion);
				return $res;
			}
	}
	
	function validatemail($email){
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);
			if(filter_var($email, FILTER_VALIDATE_EMAIL)){
				if(filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp'=> '/^.{5,50}$/')))){
					return $email;
				}
			}
			return false;
	}
	
	function debug($array){
		echo "<pre>";
		print_r($array);
		echo "</pre><br>";
	}
?>

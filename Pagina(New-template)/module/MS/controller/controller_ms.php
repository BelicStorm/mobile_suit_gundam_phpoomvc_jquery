<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_phpoomvc_jquery/Pagina(New-template)/';
    include($path . "module/MS/model/DAOMS.php");
    //D:\xamp\htdocs\htdoc_daw1\ejercicios\9\my_edit\module\MS\model\DAOMS.php
    //include ("module/MS/model/DAOMS.php");
    switch($_GET['op']){
        case 'list';
            try{
                $daoms = new DAOMS();
                if(isset($_GET['filter'])){
                    $rdo = $daoms->select_all($_GET['filter']);
                }else{
                    $rdo = $daoms->select_all("");
                }
                
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/MS/view/list_ms.php");
            }
            break;
        case 'create';
            include("module/MS/model/validate.php");
            if ($_POST) {
                $result = validate_model();
                //die(debug($result));
                //die(debug($result['datos']));
                if ($result['resultado']) {
                    try{
                        $daoms = new DAOMS();
    		            $rdo = $daoms->insert_mobile_suit($result['datos']);
                    }catch (Exception $e){
                        //print_r($result['resultado']);
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }
		            if($rdo){
                        //die(debug($result));
            			echo '<script language="javascript">alert("Registrado en la base de datos correctamente")</script>';
            			$callback = 'index.php?page=controller_ms&op=list';
        			   die('<script>window.location.href="'.$callback .'";</script>');
            		}else{
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
            		}
                }else{
                    $error = $result['error'];
                }
            }
            include("module/MS/view/create_ms.php");
            break;
        case 'update';
            include("module/MS/model/validate.php");
            if ($_POST) {
                $result = validate_model();
                //die(debug($result));
                //die(debug($result['datos']));
                if ($result['resultado']) {
                    try{
                        $daoms = new DAOMS();
                        $rdo = $daoms->update_mobile_suit($result['datos'],$_GET['modelo'],$_GET['name']);
                        
                        //die(debug($rdo));
                    }catch (Exception $e){
                        //print_r($result['resultado']);
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }
		            if($rdo){
                        //die(debug($result));
            			echo '<script language="javascript">alert("Actualizado correctamente")</script>';
            			$callback = 'index.php?page=controller_ms&op=list';
        			   die('<script>window.location.href="'.$callback .'";</script>');
            		}else{
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
            		}
                }else{
                    $error = $result['error'];
                }
            }
            try{
                $daoms = new DAOMS();
                $rdo = $daoms->select_ms($_GET['name']);
                $ms=get_object_vars($rdo);
            }catch (Exception $e){
                //$callback = 'index.php?page=503';
                //die('<script>window.location.href="'.$callback .'";</script>');
            }
            if(!$rdo){
                //$callback = 'index.php?page=503';
                //die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/MS/view/update_ms.php");
            }
            break;
        case 'read';
            try{
                $daoms = new DAOMS();
                $rdo = $daoms->select_ms($_GET['id']);
                $ms=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/MS/view/read_ms.php");
            }
            break;
        case 'delete';
            if (isset($_POST)){
                //var_dump("entra interfaz delete");
                    //borrado especifico
                if($_GET['status']==0){
                    // var_dump("entra a borrar");
                        try{
                            $daoms = new DAOMS();
                            $rdo = $daoms->delete_mobile_suit($_GET['modelo'],$_GET['name']);
                        }catch (Exception $e){
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                        
                        if($rdo){
                            echo '<script language="javascript">alert("Borrado en la base de datos correctamente")</script>';
                            $callback = 'index.php?page=controller_ms&op=list';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }else{
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                    }
                    include("module/MS/view/delete_ms.php");
                }
            break;
        case 'delete_all_ms';
            //var_dump("Llegaste");
            try{
                $daoms = new DAOMS();
                $rdo = $daoms->delete_all_mobile_suit();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if($rdo){
                echo '<script language="javascript">alert("Borrado en la base de datos correctamente")</script>';
                $callback = 'index.php?page=controller_ms&op=list';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            break;
        case 'read_modal';
            try{
                $DAOMS = new DAOMS();
                $rdo = $DAOMS->select_ms($_GET['name']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $MS=get_object_vars($rdo);
                echo json_encode($MS);
                //echo json_encode("error");
                exit;
            }
            break;
        default;
            include("view/inc/error404.php");
            break;
    }
//functions    
    function white_space(form){//valida que no hay espacios en blanco en el formulario
        var op = true;
        /* $("input:radio").each(function(){
            var name = $(this).attr("name");
            if($("input:radio[name="+name+"]:checked").length == 0){
                op = false;
            }
        }); */
        $("#"+form).find(':input').each(function() {
            var elemento= this;
            
            if(elemento.value.length==0){
                alert('Rellena los campos');
                op=false;
                if(op==false){
                    return op;
                }
            }
            
        });
        return op;
    }
    function validate_name(){
        if (!/^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/.test(document.getElementById('reg_username').value)){
            document.getElementById('e_Reg_username').innerHTML = "Tu nombre tiene que tener de 4 a 20 caracteres y no incluir _ o .";
                return 1;
            }
            document.getElementById('e_Reg_username').innerHTML = "";
            return 0;
    }
    function validate_mail(){
        if (!/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(document.getElementById('reg_email').value)){
            document.getElementById('e_Reg_email').innerHTML = "Tu Mail no es valido";
                return 1;
            }
            document.getElementById('e_Reg_email').innerHTML = "";
            return 0;
    }
    function validate_pass(){
        if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(document.getElementById('reg_password').value)){
            document.getElementById('e_Reg_password').innerHTML = "Tu contraseña tiene que contener al menos 8 caracteres,una mayuscula, una minuscula y un caracter especial";
                return 1;
            }
            document.getElementById('e_Reg_password').innerHTML = "";
            return 0;
    }
    function confirm_pass(){
        if (document.getElementById('reg_password').value!=document.getElementById('reg_confirm_password').value){
            document.getElementById('e_Reg_confirm_password').innerHTML = "Tus contraseñas no coinciden";
                return 1;
            }
            document.getElementById('e_Reg_confirm_password').innerHTML = "";
            return 0;
    }
    var ajax =function(serialized_data,option){//promesa que envia datos al conrolador segun la opcion
        return new Promise(function(resolve){
                $.ajax({
                url : "module/login_register/controller/autentication_controller.php?op="+option,
                type: 'POST',
                data : serialized_data
            }).done(function(response){ //
                //console.log(response);
                resolve(response);
            });  
            
        })
    }
    var response = function(response) {
        return new Promise(function(resolve, reject) {
            resolve(response);
        })
    };
    function registrarse(){
        //console.log($('#register_form').serialize());
         ajax($('#register_form').serialize(),'register').then(function(result){//envia los datos serializados del formulario al controlador
            response(result).then(function(result2){
                //console.log(result2);
                if(result2==-1){//dependiendo de los datos que reciva del controlador pintara errores o redireccionara
                    document.getElementById('e_global_error').innerHTML = "El Usuario Ya existe";
                }else{
                    document.getElementById('e_global_error').innerHTML = "";
                    url = "index.php?page=login_register";
                    $( location ).attr("href", url);
                } 
            });
        }); 
    }
    function login(){
        ajax($('#login-form').serialize(),'login_user').then(function(result){//comprueba que el usuario existe
            //console.log(result);
            if(result==1){//si existe comprueba que la contraseña cifrada coincide con la de base de datos
                ajax($('#login-form').serialize(),'login_pass').then(function(result){
                    document.getElementById('e_login_global_error').innerHTML = "";
                    //console.log(result);
                    if(result==1){ //si existe y coincide con su conraseña guarda en sesion lo datos necesarios
                        document.getElementById('e_login_global_error').innerHTML = "";
                        ajax($('#login-form').serialize(),'save_in_session').then(function(result){
                            console.log(result);
                            url = "index.php";
                            $( location ).attr("href", url);
                        }) 
                    }else{
                            document.getElementById('e_login_global_error').innerHTML = "El usuario no existe";
                    } 
                }) 
            }else{
                    document.getElementById('e_login_global_error').innerHTML = "El usuario o contraseña erroneos";
            }
        }) 
    }
    var profile_pics_get_api = new Promise (function(resolve,reject){ //obtiene las imagenes necesarias para el registro
        $.ajax({
            url : "http://localhost:3000/profile_pics",
            type: 'get',
        }).done(function(response){ //
            //console.log(response[0].url);
            resolve(response);
        });  
    });
    var get_session_profile_pic = new Promise (function(resolve,reject){ //obtiene la imagen de perfil del usuario logeado
        $.ajax({
            url : "module/login_register/controller/autentication_controller.php?op=get_profile_pic",
            type: 'get',
            data: { get_param: 'value' }, 
            dataType: 'json'
        }).done(function(response){ //
            //console.log(response[0].url);
            resolve(response);
        });  
    });
    var get_session_user_name = new Promise (function(resolve,reject){ //obtiene el nombre del usuario logeado
        $.ajax({
            url : "module/login_register/controller/autentication_controller.php?op=get_user_name",
            type: 'get',
            data: { get_param: 'value' }, 
            dataType: 'json'
        }).done(function(response){ //
            //console.log(response[0].url);
            resolve(response);
        });  
    });
//functions
var consultas = function(option) {
        return new Promise(function(resolve, reject) {
            $.ajax({ 
                type: 'GET', 
                url: "module/cart_and_purchases/controller/purchase_controller.php?op="+option, 
                data: { get_param: 'value' }, 
                dataType: 'json',
            })
            .done(function( data, textStatus, jqXHR ) {
                //console.log('ok');
                resolve(data);
            })
            
        })
    };

$(document).ready(function(){
	var ok= 0;
    var content = "";
     profile_pics_get_api.then(function(result){ //pinta en el formulario de registro las imagenes de perfil
        /* console.log(result[0].url); */
        if(result!='no'){
            $.each(result, function(index, list) {
                content += '<div id="profile_pic"><img src="'+list.url+'"></div>';
                content += '<input type="radio" name="user_pic" value="'+list.url+'">'
            });
        }else{
                content += '<span>Sin imagenes disponibles</span>';
        }
        //console.log(content);
        $('.user_select_avatar').append(content);
     })
     window.addEventListener('offline', function(){//si detecta que la conexion se ha perdido se deslogueara
            Promise.all([consultas("stored_shop_cart"), ajax("",'destroy_session')]).then(responses => {
                location.reload(); 
                url = "index.php";
                $( location ).attr("href", url); 
            });
        }) 
    //validations  
        $('#reg_username').on('focus blur change', function() {
            ok=validate_name();
        });
        $('#reg_email').on('focus blur change', function() {
            ok=validate_mail();
        });
        $('#reg_password').on('focus blur change', function() {
            ok=validate_pass();
        });
        $('#reg_confirm_password').on('focus blur change', function() {
            ok=confirm_pass();
        });
    //validations
    //register   
        $('#register_form').keydown(function(e) {
            var key = e.which;
            if (key == 13) {
                if(white_space('register_form')){
                    if(ok==0){
                        //console.log(generic_ajax($('#register_form').serialize()));
                        registrarse();
                    }   
                }
            }
        }); 
        $("#register-submit").on('click',function(){
            if(white_space('register_form')){
                if(ok==0){
                    registrarse();
                }  
            }
        }); 
    //register
    //login
        $('#login_submit').on('click',function(){
            if(white_space('login-form')){
                console.log($('#login-form').serialize());
                    login();
            }
        });
        $('#login-form').keydown(function(e) {
            var key = e.which;
            if (key == 13) {
                if(white_space('login-form')){
                    if(ok==0){
                        login();
                    }   
                }
            }
        }); 
    //login 
    //logout
        $('#logout').on('click',function(){//si se deslogea se destruye la sesion de todos los datos almacenados
            consultas("stored_shop_cart").then(function(resolve){
                ajax("",'destroy_session').then(function(resolve){
                    url = "index.php?page=login_register";
                    $( location ).attr("href", url);
                     location.reload(); 
                })  
            });
            //console.log(result);   
        });
    //logout 
    //print_user_avatar
        get_session_profile_pic.then(function(result){//obtiene de sesion la imagen del usuario logeado
            //console.log(result);
            if(result!="no"){
                get_session_user_name.then(function(result2){
                    $('#user_avatar').append('<img src="'+result.avatar+'"> <span>'+result2+'</span>');
                })
               //console.log(result.avatar);
            }
        });
    //print_user_avatar 
    
});

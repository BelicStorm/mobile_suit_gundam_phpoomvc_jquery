<?php
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_phpoomvc_jquery/Pagina(New-template)/';
include($path . "module/login_register/module/autenticationDAOMS.php");
switch($_GET["op"]){
    case "register":
        //obten los datos serializados, encripta la contraseña y lo envia todo al Dao para que registre al usuario
        //Si existe devolvera un -1 y js pintara los errores.
        $user_name= $_POST['username']; 
        $email=     $_POST['email'];
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $avatar=  $_POST['user_pic'];
            try{
                $DAOMS = new Auth_DAOMS();
                $rdo = $DAOMS->register($user_name,$email,$password,$avatar);
            }catch (Exception $e){
                return json_encode("error");
            }
            if(!$rdo){
                return json_encode("error");
            }else{
            // var_dump(json_encode($rdo));
            if($rdo){
                    echo json_encode($rdo);
                
                }
                
            }
    break;
    case "login_user":
    @session_start();
        //comprueba si el usuario enviado existe (1 existe)
        $user_name= $_POST['username'];
        
            //echo json_encode($password);
            try{
                $DAOMS = new Auth_DAOMS();
                $rdo = $DAOMS->login($user_name);
            }catch (Exception $e){
                return json_encode("error");
            }
            // var_dump(json_encode($rdo));
            if($rdo){
                
                echo json_encode($rdo);
                
            }
    break;
    case "login_pass":
        //comprueba que la contraseña cifrada enviada es igual a la del usuario que pretende logearse
        $user_name= $_POST['username'];
        $password = $_POST['password'];
            try{ //obtener imagen avatar
                $DAOMS = new Auth_DAOMS();
                $rdo = $DAOMS->get_info_pass($user_name);
            }catch (Exception $e){
                return json_encode("error");
            }
            if($rdo){
                //var_dump($rdo);
                
                if (password_verify( $password, $rdo)) {
                    echo json_encode(1);
                } else {
                    echo json_encode("error");
                }
                
            }
    break;
    case "save_in_session":
        @session_start();
        //Guarda en sesion el nombre de usuario, su avatar, y su tipo
         $user_name= $_POST['username'];
         $_SESSION["user"]=$user_name;
            try{ //obtener imagen avatar
                $DAOMS = new Auth_DAOMS();
                $rdo = $DAOMS->get_info_user($user_name);
            }catch (Exception $e){
                return json_encode("error");
            }
            if($rdo){
                $_SESSION['profile_avatar']=json_encode(get_object_vars($rdo));    
            }
        if($user_name=="Amuro Ray"){ //Asigna su tipo dependiendo del nombre
            $_SESSION["type"]="1";
        }else{
            $_SESSION["type"]="0";
        }
       echo $_SESSION['profile_avatar'];
    break;
    case "destroy_session":
        @session_start();
        unset($_SESSION['user']);
        unset($_SESSION['type']);
        unset($_SESSION['profile_avatar']);
        unset($_SESSION['productos']);
        session_destroy();
        echo json_encode('ok');
    break;
    case "get_user_name":
        @session_start();
        if(isset($_SESSION['user'])){
            echo json_encode($_SESSION['user']);
        }else{
            echo json_encode("Ningun usuario en session");
        }
        
    break;
    case "get_profile_pic":
         @session_start();
        if(isset($_SESSION['user'])){
            echo $_SESSION['profile_avatar'];
        }else{
            echo "no";
        }
    break;
}
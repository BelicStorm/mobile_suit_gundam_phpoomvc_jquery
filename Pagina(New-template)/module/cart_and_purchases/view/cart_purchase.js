//promesas
    var in_cart = new Promise (function(resolve,reject){
        if(jQuery(location).attr('href')=="http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_phpoomvc_jquery/Pagina(New-template)/index.php?page=cart"){
            resolve(1); 
        }
        resolve(0);
    });
    
    var consultas_modificar = function(option,modelo,name,qty) {
        return new Promise(function(resolve, reject) {
            $.ajax({ 
                type: 'GET', 
                url: "module/cart_and_purchases/controller/purchase_controller.php?op="+option +
                                        "&modelo="+modelo+"&ms_name="+name+"&qty="+qty, 
                data: { get_param: 'value' }, 
                dataType: 'json',
            })
            .done(function( data, textStatus, jqXHR ) {
                //console.log('ok');
                resolve(data);
            })
        })
    };
//promesas
//funciones
    function items_in_cart(){
        document.getElementById('ex4').innerHTML = "";
        consultas_modificar("get_products","","","").then(function(result){
            console.log(result.length);
            if(result!=0){
            document.getElementById('ex4').innerHTML = '<i class="p3 fa fa-shopping-cart  xfa-inverse" ></i><span class="p1 fa-2x has-badge" id="count_cart" data-count="'+result.length+'"></span>';
            }else{
                document.getElementById('ex4').innerHTML = '<i class="p3 fa fa-shopping-cart  xfa-inverse" ></i><span class="p1 fa-2x has-badge" id="count_cart" data-count="0"></span>';
            }
        })
    }
    function print_cart(){
        var content = "<table id='carrito'><tr><th>Modelo</th><th>Nombre</th><th>Precio</th><td></td><td></td></tr>";
        var precio = 0;
            consultas_modificar("get_products","","","").then(function(result){
                console.log(result);
                //
                if(result!=0){
                    $.each(result, function(key1, value1) {
                    //console.log(value1);
                        content += "<tr>";
                        content += "<td>"+value1.modelo+"</td>";
                        content += "<td>"+value1.ms_name+"</td>";
                        content += "<td>"+value1.precio+"</td>";
                        content += "<td><div id='field1'><button type='button' id='sub "+value1.modelo+" "+value1.ms_name+"' class='sub'>-</button>";
                        content += "<input type='number' id='1' value='"+value1.cantidad+"' min='1' max='3' readonly/>"+
                        "<button type='button' id='add "+value1.modelo+" "+value1.ms_name+"' class='add'>+</button></div></td>";
                        content += "<td><span class='fa fa-trash' id='"+value1.modelo+" "+value1.ms_name+"'></td>";
                        content += "</tr>";
                        precio = precio + parseInt(value1.precio)*parseInt(value1.cantidad);
                    });
                    content += "<tr><td></td><td></td><td>"+precio+"</td><td></td>"+
                    "<td></td></tr>";
                    content += "<tr><td></td><td></td><td></td><td></td>"+
                    "<td><button type='button' class='btn btn-outline-success' id='comprar'>Comprar</button></td></tr></table>"
                }else{
                    content = "Ningun Objeto en el carrito";
                }
                
                document.getElementById('body_cart').innerHTML = content;
            })
    };
    function update_qty(modelo,name,qty){
        consultas_modificar('update_qty',modelo,name,qty).then(function(resolve){
            //console.log(resolve);
            location.reload();
        })
    }
//funciones
$(document).ready(function(){
//redirecciones
    $('#BSC').on('click',function(){
        url = "index.php?page=shop";
        $( location ).attr("href", url);
    })
    $('#cart').on('click',function(){
        url = "index.php?page=cart";
        $( location ).attr("href", url);
    })
//redirecciones 
//añadir productos al carro
 items_in_cart();
    $('.fa-plus').on('click',function(){
        var modelo = $(this).attr('id').split(' ')[0];
        var name = $(this).attr('id').split(' ')[1];
        console.log(modelo+name);
        consultas_modificar("add_product",modelo,name,"").then(function(result){
            console.log(result);
             items_in_cart();
        })
    });
//añadir productos al carro
//pintar carrito en su pagina
    $('#body_cart').on('click', '.fa-trash', function() {
        var modelo = $(this).attr('id').split(' ')[0];
        var name = $(this).attr('id').split(' ')[1];
            consultas_modificar("delete_product",modelo,name,"").then(function(result){
                            /* alert(result); */
                location.reload();
            })
    });
    in_cart.then(function(result){
        if(result==1){
            print_cart();
        }
    })
     
        
//pintar carrito en su pagina
//modificar objetos del carro
    $('#body_cart').on('click','.add',function () {
		if ($(this).prev().val() < 3) {
    	    $(this).prev().val(+$(this).prev().val() + 1);
            var modelo = $(this).attr('id').split(' ')[1];
            var name = $(this).attr('id').split(' ')[2];
            var qty =  $(this).prev().val();
            //console.log(qty+name+modelo);
            update_qty(modelo,name,qty);
		}
    });
    $('#body_cart').on('click','.sub',function () {
		if ($(this).next().val() > 1) {
    	    $(this).next().val(+$(this).next().val() - 1);
            var modelo = $(this).attr('id').split(' ')[1];
            var name = $(this).attr('id').split(' ')[2];
            var qty =  $(this).next().val();
            //console.log(qty+name+modelo);
            update_qty(modelo,name,qty);
		}
    });
//modificar objetos del carro
});

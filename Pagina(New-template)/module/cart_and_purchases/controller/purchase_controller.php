<?php
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_phpoomvc_jquery/Pagina(New-template)/';
include($path . "module/cart_and_purchases/model/purchase_DAOMS.php");

function searchForIt($array,$ms_name) {
    $op=true;
   foreach ($array as $v1) {
        foreach ($v1 as $v2) {
            if($v2 == $ms_name){
                $op=false;
                break;
            }
        }
    }
   return $op;
}
switch ($_GET['op']) {
//carro de la compra
    case 'add_product':
        $array = [
                "action" => "select_product",
                "modelo" => $_GET['modelo'],
                "ms_name"=> $_GET['ms_name']
            ];
        //var_dump($array);
        try{
            $daoms = new purchase_DAOMS();
            $rdo = $daoms->actions_get($array);
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        if($rdo){
            //var_dump($rdo->price_per_unit);
            $precio=$rdo->price_per_unit;
            @session_start();
            /* session_destroy();  */
            if(isset($_SESSION['Productos'])){
                if(searchForIt($_SESSION['Productos'],$_GET['ms_name'])){
                    $array_productos=["modelo" => $_GET['modelo'],"ms_name"=> $_GET['ms_name'],"precio"=>$precio,"cantidad"=>1];
                    $_SESSION['Productos'][] =  $array_productos;
                    echo json_encode($_SESSION['Productos']);
                }  
            }else{
                    $array_productos=["modelo" => $_GET['modelo'],"ms_name"=> $_GET['ms_name'],"precio"=>$precio,"cantidad"=>1];
                    $_SESSION['Productos'][] =  $array_productos;
                    echo json_encode($_SESSION['Productos']);
            }
             
        }
        break;
    case 'get_products':
        @session_start();
            /* session_destroy();  */
            if(isset($_SESSION['Productos'])){
                if(isset($_SESSION['user'])){
                   if(isset($_SESSION['login_cart'])){
                       echo json_encode($_SESSION['Productos']);
                   }else{
                        $_SESSION['login_cart']=1;
                    try{
                        $daoms = new purchase_DAOMS();
                        $rdo = $daoms->actions_get_logged_cart($_SESSION['user']);
                        
                    }catch (Exception $e){
                        echo json_encode("error");
                        exit;
                    }
                        if($rdo){
                            $ok=0;
                            foreach ($rdo as $keyA => $valueA) {
                                foreach ($_SESSION['Productos'] as $keyB => $valueB) {
                                    if ($valueA['ms_name']==$valueB['ms_name']) {
                                        $_SESSION['Productos'][$keyB]['cantidad']=$_SESSION['Productos'][$keyB]['cantidad']+$valueB['cantidad']; 
                                        $ok=1;
                                        break;  
                                    }
                                }
                                    if ($ok==0) {
                                        array_push($_SESSION['Productos'], $valueA);
                                    }else{$ok=0;}
                            }  
                            echo json_encode($_SESSION['Productos']);
                        }
                   }
                }else{echo json_encode($_SESSION['Productos']);}
            }else{
                 @session_start();
                if(isset($_SESSION['user'])){
                    $_SESSION['login_cart']=1;
                    try{
                        $daoms = new purchase_DAOMS();
                        $rdo = $daoms->actions_get_logged_cart($_SESSION['user']);
                        
                    }catch (Exception $e){
                        echo json_encode("error");
                        exit;
                    }
                    if($rdo){
                        	$dinfo = array();
                            foreach ($rdo as $row) {
                                array_push($dinfo, $row);
                            }
				        $_SESSION['Productos']=$dinfo;
				        echo json_encode($_SESSION['Productos']);
                    }else{
                        echo json_encode(0);
                    }
                }else{echo json_encode(0);}
            }
        break;
    case 'delete_product':
        @session_start();
            /* session_destroy();  */
            $ms_name = $_GET['ms_name'];
            foreach($_SESSION['Productos'] as $key => $sub_array) {
                if($sub_array['ms_name'] == $ms_name) {
                    unset($_SESSION['Productos'][$key]);
                    break; //if there will be only one then break out of loop
                }
            }
          $_SESSION['Productos']=array_values($_SESSION['Productos']);
        echo json_encode($_SESSION['Productos']);
        break;
    case 'update_qty':
        @session_start();
            /* session_destroy();  */
            $ms_name = $_GET['ms_name'];
            $qty = $_GET['qty'];
            if($qty<1) {
                $qty=1;
            }
            if($qty>10) {
                 $qty=10;
            }
            foreach($_SESSION['Productos'] as $key => $sub_array) {
                if($sub_array['ms_name'] == $ms_name) {
                    $_SESSION['Productos'][$key]['cantidad']= $qty;
                    break; //if there will be only one then break out of loop
                }
            }
          $_SESSION['Productos']=array_values($_SESSION['Productos']);
        echo json_encode($_SESSION['Productos']);
        break;
    case 'stored_shop_cart':
     @session_start();
        try{
            $daoms = new purchase_DAOMS();
            $rdo = $daoms->create_user_shop_cart($_SESSION['Productos'],$_SESSION['user']);
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode("ok");
        break;
//carro de la compra
//comprar
    case 'purchase':
        @session_start();
        try{
            $daoms = new purchase_DAOMS();
            $rdo = $daoms->purchase($_SESSION['Productos'],$_SESSION['user']);
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode("ok");     
        break;
    case 'destroy_shop_cart':
        @session_start();
         $user=$_SESSION['user'];
         $type=$_SESSION['type'];
         $avatar=$_SESSION['profile_avatar'];
         unset($_SESSION['user']);
        unset($_SESSION['type']);
        unset($_SESSION['profile_avatar']);
        unset($_SESSION['productos']);
        session_destroy();
        @session_start();
        $_SESSION['user']=$user;
        $_SESSION['type']=$type;
        $_SESSION['profile_avatar']=$avatar;
        echo json_encode("ok");
        break;
    
//comprar
    default:
        # code...
        break;
}

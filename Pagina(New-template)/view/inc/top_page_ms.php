<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Industrias</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
    	<script type="text/javascript">
        	$(function() {
        		$('#date').datepicker({
        			dateFormat: 'dd/mm/yy', 
        			changeMonth: true, 
        			changeYear: true, 
        			yearRange: '2019:2030',
        			onSelect: function(selectedDate) {
        			}
        		});
        	});
		</script>
		<script type="text/javascript" src="view/js/key_exclude.js"></script>	
		<!--Datatable-->
			<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
			<script src="module/MS/view/js/view.js"></script>
			<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

		<!--Mis links-->
			<script src="module/social_accions/view/like_dislike.js"></script>
			<link rel="stylesheet" href="view/css/my_styles.css" />
			<script src="module/MS/model/validate_MS.js"></script>
			<script src="module/MS/model/validate_MS_list_filter.js"></script>
			<script src="module/MS/model/validate_MS_buttons.js"></script>
			<script src="module/MS/model/modal_read.js"></script>
			<script src="view/js/general_lang.js"></script>
			<script src="view/js/direction_controller.js"></script>
			<script src="module/login_register/view/autentication.js"></script>
		
		
		<!--JqWidgets-->
			<link href="https://jqwidgets.com/public/jqwidgets/styles/jqx.base.css" rel="stylesheet" type="text/css" />
			<script src="https://jqwidgets.com/public/jqwidgets/jqx-all.js"></script>
			<script src="module/all_products/model/all_p_datatable.js"></script>

		<!--templates-->
		<!-- Custom Theme files -->
			<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
			<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> 
			<link href="view/css/bootstrap.css" rel='stylesheet' type='text/css' /> 
		
			<link href="view/css/style.css" rel='stylesheet' type='text/css' />
			<link href="view/css/font-awesome.css" rel="stylesheet"> 			<!-- font-awesome icons -->
		<!-- //Custom Theme files -->
		<!-- fonts -->
			<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
			<link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
		<!-- //fonts -->
		<!-- js -->
			<script src="view/js/bootstrap.js"></script> 
			<script src="view/js/menu_jquery.js"></script> <!-- pop-up -->
			<script type="text/javascript" src="view/js/move-top.js"></script>
			<script type="text/javascript" src="view/js/easing.js"></script>
			<script type="text/javascript" src="view/js/template_js.js"></script>	
		<!-- //js -->

	    <!--- maps --->
			<script type="text/javascript" src="module/maps_&_references/view/js/maps.js"></script>	
			<link rel="stylesheet" href="view/css/maps.css" />
			
		<!-- //maps -->

		<!-- shop module -->
			<script type="text/javascript" src="module/inicio/view/js/shop_search.js"></script>	
			<script type="text/javascript" src="module/shop/view/shop_view.js"></script>
			<script src="module/cart_and_purchases/view/cart_purchase.js"></script>
			<script src="module/cart_and_purchases/view/purchase.js"></script>		
		<!-- //shop module -->
		<!-- api pruebas -->
			<script  src="module/inicio/view/js/api_pruebas.js"></script>	
			<script  src="module/api_ebay/view/recomended_products_api.js"></script>
		<!-- api pruebas -->
		<!--Toasters-->
			
		<!--Toasters-->
    </head>
    <body>


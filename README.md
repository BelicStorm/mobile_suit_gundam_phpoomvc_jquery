# Mobile_suit_Gundam_PHPOOMVC_JQUERY


## **Resumen**
- Proyecto que emula una pagina de compraventa de productos ficticios de la serie Mobile Suit Gundam del periodo Universal Century.

## **Paginas**
1. Vista Administrador
    1. Home
    2. MS (CRUD Productos de la pagina)
    3. All_products(Datatable)
    4. Productos
    5. Conocenos
    6. Carro de la compra
    7. Logout
2. Usuario Logeado
    1. Logout
    2. Home
    3. Productos
    4. Conocenos
    5. Carro de la compra
3. Visitante
    1. Login -- Register
    2. Home
    3. Productos
    4. Conocenos

## **Tecnologias Usadas y Requeridas**
1. PHP 5
2. Bootstrap 3
3. jQuery
4. Mysql Server
5. Json Server
6. JqWidgets
	1. Datatable
	2. Autocomplete
6. API Ebay 

## **BDD**
![BDD](README_Media/1.png)

Las tablas Plantilla_historial_productos, plantilla_historial_compras y 
temp_shop_cart son tablas de referencia para tablas que se crearan
dinamica durante el uso de la pagina para cada usuario:
* Plantilla_historial_productos y plantilla_historial_compras:
    * Las Tablas se generan cunado el usuario compra. La primera
    almacenara los productos de la compra y la segunda los datos de la compra.
* temp_shop_cart:
    * La tabla generada a sera el carro de la compra del usuario.

La tabla log_table, almacena las acciones que repercuten a un usuario logeado
cuando este no lo esta.

## **Mejoras**
| Modulo                    | FUNCIONALIDADES |
| ------                    | ------ |
| Language                  | Cambia de forma dinamica el idioma obteniendolo de unos ficheros .json |
| Home                      | Carrusel |
| SEARCH                    | Busqueda con filtro dependiente, no requiere de todos los parametros para buscar |
| MS                        | Bootstrap Datatable, Solucion al problema del elemento 10 datatable, modal, CRUD y read Modal | 
| All_products              | Jqwidget Datatable | 
| Productos                 | Extraer de base de datos los productos listados y individualmente (Las imagenes se obtienen de JSON Server) | 
| Carro de la compra        | Añadir producto al carro(Logeado y sin logear),actualizar cantidad o borrar productos, controlar el carro de la compra dependiendo del estado del usuario(Logeado o visitante): Si el visitante añade productos al carro al logearse se actualizaran las cantidades de los productos que ya esten y añadira los inexistentes | 
| Comprar                   | Control de usuario logeado(un visitante no puede comprar), al comprar vacia el carro tanto en la pagina como en BDD, al comprar se generan datos en el historial de compra |
| Login                     | Control de usuarios en BDD, contraseña cifrada, contraseña de 8 caracteres con mayuscula, minuscula, numero y caracter alfanumerico | 
| Register                  | Validacion de contenido en blanco, rango del nombre, expresion regular de la contraseña y mail, seleccionar imagen de perfil(API JSON Server), Control de duplicidad en usuarios | 
| Logout                    | Comprueba que hay productos en el carro y al hacer logout los añade al carro personal del usuario, Logout automatico al perder la conexion | 
| Like                      | Comprueba que el user este logeado(Si el usuario no se encuentra logeado lo redirige al login y si este se logea insertara o borrara automaticamente el like, si no este se perdera), quitar likes, cada user ve sus likes marcados y la suma de todos los que tiene el producto | 
| API  EBAY                 | Productos recomendados desde ebay |
| API  JSON                 | La imagenes de los productos y las imagenes de perfil por defecto se cargan desde json server |
| API  GOOGLE Maps          | Carga el mapa en el footer |
| BDD                       | Creacion de forma dinamica de tablas unicas para cada usuario(Historial de compra, carro de la compra etc) |

## **Links**

[JqueryInput Autocomplete](https://www.jqwidgets.com/jquery-widgets-demo/demos/jqxinput/index.htm#demos/jqxinput/defaultfunctionality.htm)
[Jquery Datatable](https://www.jqwidgets.com/jquery-widgets-demo/demos/jqxdatatable/index.htm)

